/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/UnitTests/JUnit5TestClass.java to edit this template
 */
package com.mycompany.lab3;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

/**
 *
 * @author Love_
 */
public class OXUnitTest {
    
    public OXUnitTest() {
    }
    
    @BeforeAll
    public static void setUpClass() {
    }
    
    @AfterAll
    public static void tearDownClass() {
    }
    
    @BeforeEach
    public void setUp() {
    }
    
    @AfterEach
    public void tearDown() {
    }

    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    // @Test
    // public void hello() {}
    @Test
    public void testCheckWinIsFalse() {
        char turn = 'X';
        char[][] board = {{'X','O','-'},{'X','X','O'},{'-','-','-'}};
        assertEquals(false,OXProgram.IsWin(board, turn));
    }
    
    @Test
    public void testCheckXWinIsRowTrue() {
        char turn = 'X';
        char[][] board = {{'-','O','-'},{'X','X','X'},{'-','-','O'}};
        assertEquals(true,OXProgram.IsWin(board, turn));
    }

    @Test
    public void test2CheckXWinIsRowTrue() {
        char turn = 'X';
        char[][] board = {{'X','X','X'},{'X','O','O'},{'O','-','-'}};
        assertEquals(true,OXProgram.IsWin(board, turn));
    }
    
    @Test
    public void testCheckOWinDiagonaIsWinTrue() {
        char turn = 'O';
        char[][] board = {{'O','-','-'},{'-','O','-'},{'-','-','O'}};
        assertEquals(true,OXProgram.IsWin(board, turn));
    }
    
    @Test
    public void testCheckOWinDiagonaIsWinFalse() {
        char turn = 'O';
        char[][] board = {{'O','-','-'},{'-','O','-'},{'-','-','X'}};
        assertEquals(false,OXProgram.IsWin(board, turn));
    }
    
    @Test
    public void testCheckXWinDiagonaIsWinTrue2() {
        char turn = 'X';
        char[][] board = {{'-','-','X'},{'-','X','-'},{'X','-','-'}};
        assertEquals(true,OXProgram.IsWin(board, turn));
    }
    
    @Test
    public void testCheckXWinDiagonaIsWinFalse2() {
        char turn = 'X';
        char[][] board = {{'-','-','X'},{'-','O','-'},{'X','-','-'}};
        assertEquals(false,OXProgram.IsWin(board, turn));
    }

    @Test
    public void testCheckOWinColIsWinTrue() {
        char turn = 'O';
        char[][] board = {{'O','-','-'},{'O','X','-'},{'O','-','X'}};
        assertEquals(true,OXProgram.IsWin(board, turn));
    }
     @Test
    public void testCheckOWinColIsWinTrue2() {
        char turn = 'O';
        char[][] board = {{'-','O','-'},{'-','O','-'},{'-','O','-'}};
        assertEquals(true,OXProgram.IsWin(board, turn));
    }
     @Test
    public void testCheckOWinColIsWinTrue3() {
        char turn = 'O';
        char[][] board = {{'-','-','O'},{'-','-','O'},{'-','-','O'}};
        assertEquals(true,OXProgram.IsWin(board, turn));
    }
    @Test
    public void testCheckOWinColIsWinFalse() {
        char turn = 'O';
        char[][] board = {{'O','-','-'},{'X','X','-'},{'O','-','X'}};
        assertEquals(false,OXProgram.IsWin(board, turn));
    }
    @Test
    public void testCheckBoardFullByFalse() {
        char[][] board = {{'O','X','O'},{'X','-','O'},{'O','O','X'}};
        assertEquals(false,OXProgram.IsBoardFull(board));
    }
    @Test
    public void testCheckBoardFullByTrue() {
        char[][] board = {{'O','X','O'},{'X','X','O'},{'O','O','X'}};
        assertEquals(true,OXProgram.IsBoardFull(board));
    }
    
    @Test
    public void testCheckBoardFullByFalse2() {
        char[][] board = {{'-','-','-'},{'-','-','-'},{'-','-','-'}};
        assertEquals(false,OXProgram.IsBoardFull(board));
    }
}
